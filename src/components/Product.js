import React, {useState, useEffect, useContext} from 'react';

//react-bootstrap components

import {Row, Col, Card, Button, CardGroup} from 'react-bootstrap'

import UserContext from '../userContext'
import Swal from 'sweetalert2'
import image6 from '../images/img6.jpg'
import ListGroup from 'react-bootstrap/ListGroup'
import ListGroupItem from 'react-bootstrap/ListGroupItem'


export default function Product({productProp}){
	//console.log(courseProp);


const [count, setCount] = useState(0)
const [seats, setSeats] = useState(5);
const [isActive, setIsActive] = useState(true)
const [login, setLogin] = useState(false)

const {user, setUser} = useContext(UserContext)


// Will check if seats is 0, set the isActive state to flase and conditionally render the button as disabled everytime our component re-renders
useEffect(() => {
	if(seats === 0){
		setIsActive(false)
	}
},[seats])



function enroll(){
	setCount(count + 1)
	setSeats(seats - 1)
}

function isLogin(){
	setLogin(login)
	Swal.fire({
				icon: "error",
				title: "Can't Buy Product",
				text: "Please login to buy product"
			})
}




	return (
			<Col>	
				<Card className="cardProd" style={{ width: '18rem' }}>
				  <Card.Img variant="top" src={productProp.img} />
				  <Card.Body>
				    <Card.Title>{productProp.productName}</Card.Title>
				    <Card.Text>
				      {productProp.description}
				    </Card.Text>
				  </Card.Body>
				  <ListGroup className="list-group-flush">
				    <ListGroupItem>PRICE: {productProp.price} PHP</ListGroupItem>
				    <ListGroupItem>
				    	STATUS: {
				    		seats <= 0 
				    		? <span className="text-danger">Out of Stock</span>
				    		: <span className="text-success">Still Available</span>
				    	}
				    </ListGroupItem>
				  </ListGroup>
				  <Card.Body>
				   		{
				   		user.email
				   		?
				   			isActive === false

				   				? <Button variant="danger" dsiabled block>Unavailable</Button>
				   				: <Button variant="dark" className="color-button" onClick={enroll} block>ORDER</Button>
				   		: <Button variant="dark" className="color-button" onClick={isLogin} block>ORDER</Button>
				   		}
				  </Card.Body>
				</Card>
				</Col>
	)
}

