import React, {useState, useEffect, useContext} from 'react'
import '../App.css';

/*bootstrap components*/
import {Form, Button, Row, Col} from 'react-bootstrap'
import UserContext from '../userContext'

import {Container} from 'react-bootstrap'

/*Sweet alert*/
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'

import Modal from 'react-bootstrap/Modal'

export default function Register(){

	/*states - to store value of form inputs*/
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password, setPassword] = useState("")
	const [confirmPassword, setConfirmPassword] = useState("") 
	const [willRedirect, setWillRedirect] = useState(false)



	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext)


	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	useEffect(() => {

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){
		
				setIsActive(true);

		} else {
	
				setIsActive(false);

		}


	},[firstName, lastName, email, mobileNo, password, confirmPassword]);


	function registerUser(e){
	/*submit event has a default behavior, for now, it will refresh the page and thereforem we lose our information*/
		e.preventDefault();
		console.log("This page will no longer refresh")
		/*Good Practice: Check the states before passing to fetch*/
		console.log(firstName)	
		console.log(lastName)	
		console.log(email)	
		console.log(mobileNo)	
		console.log(password)	


		fetch('https://mighty-crag-87517.herokuapp.com/api/register', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})

		}).then(res => res.json()) //response should be parsed into an object to be used in JS, that is with the use of .json()
			.then(data => {
				console.log(data)
	

				if(data.message){
					Swal.fire({
						icon: "error",
						title: "Registration Failed",
						text: data.message
					})
				} else {
					setWillRedirect(true);

					Swal.fire({
						icon: "success",
						title: "Registration Successful!",
						text: "You are now a certified GWAPO"
					})
				}
			})


			//reset input states/ states bound to input to their initial values
			setFirstName("");
			setLastName("");
			setEmail("");
			setMobileNo("");
			setPassword("");
			setConfirmPassword("");
	}

	return (

		user.email || willRedirect
		?
		<Redirect to ='/login' />
		:
		<Container>
		<Row className="mt-5">
			<Col>
			<h1>Register Here</h1>
			<Form className="mt-5" onSubmit = {e => registerUser(e)}>	
				<Form.Group>	
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => {setFirstName(e.target.value)}} required />

					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => {setLastName(e.target.value)}} required />

					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => {setEmail(e.target.value)}} required />

					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 digit Mobile Number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} required />

					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => {setPassword(e.target.value)}} required />

					<Form.Label>Verify Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} required />
				</Form.Group>
			{
				isActive
				? <Button variant="dark" className="color-button" type="submit" block>Register</Button>
				: <Button variant="dark" className="color-button" disabled block>Register</Button>


			}	
			
			</Form>	
			</Col>
		</Row>
{/*		<Button variant="primary" onClick={handleShow}>
		  Register
		</Button>
*/}
		{/*<Modal
		  show={show}
		  onHide={handleClose}
		  backdrop="static"
		  keyboard={false}
		>
		  <Modal.Header closeButton>
		    <Modal.Title>Modal title</Modal.Title>
		  </Modal.Header>
		  <Modal.Body>
		    <Form className="mt-5" onSubmit = {e => registerUser(e)}>	
		    				<Form.Group>	
		    					<Form.Label>First Name:</Form.Label>
		    					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => {setFirstName(e.target.value)}} required />

		    					<Form.Label>Last Name:</Form.Label>
		    					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => {setLastName(e.target.value)}} required />

		    					<Form.Label>Email:</Form.Label>
		    					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => {setEmail(e.target.value)}} required />

		    					<Form.Label>Mobile Number:</Form.Label>
		    					<Form.Control type="number" placeholder="Enter 11 digit Mobile Number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} required />

		    					<Form.Label>Password:</Form.Label>
		    					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => {setPassword(e.target.value)}} required />

		    					<Form.Label>Verify Password:</Form.Label>
		    					<Form.Control type="password" placeholder="Enter Password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} required />
		    				</Form.Group>		
		    			</Form>	
		  </Modal.Body>
		  <Modal.Footer>
		    {
		    	isActive
		    	? <Button variant="dark" className="color-button" type="submit" block>Register</Button>
		    	: <Button variant="dark" className="color-button" disabled block>Register</Button>


		    }
		    <Button variant="secondary" onClick={handleClose} block>
		      Close
		    </Button>
		  </Modal.Footer>
		</Modal>*/}
		</Container>
		

		)

	
}

