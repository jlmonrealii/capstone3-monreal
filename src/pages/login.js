import React, {useState, useEffect, useContext} from 'react'
import '../App.css';
import {Form, Button, Row, Col} from 'react-bootstrap'
import Swal from 'sweetalert2'
import UserContext from '../userContext'


import {Redirect} from 'react-router-dom'
import {Container} from 'react-bootstrap'


export default function Login(){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)

	//State redirection
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {

		if(email !== "" && password !== ""){
		
				setIsActive(true);

		} else {
	
				setIsActive(false);

		}


	},[email, password]);

	

	function loginUser(e){
		e.preventDefault();



		fetch('https://mighty-crag-87517.herokuapp.com/api/login', {

			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		}).then(res => res.json()) 
			.then(data => {
				console.log(data)

				if(data.message){
					Swal.fire({
						icon: "error",
						title: "Login Failed",
						text: data.message
					})
				} else {
					console.log(data.accessToken)

					localStorage.setItem('token', data.accessToken)
					fetch('https://mighty-crag-87517.herokuapp.com/api/profile', {
							headers: {
								Authorization: `Bearer ${data.accessToken}`
							}

					})
					.then(res => res.json())
					.then(data => {
						console.log(data)
						localStorage.setItem('email', data.email)
						localStorage.setItem('isAdmin', data.isAdmin)
						setUser({
							email: data.email,
							isAdmin: data.isAdmin
						})

						setWillRedirect(true)

						Swal.fire({
						icon: "success",
						title: "Login Successful!",
						text: `Thank you for logging in, ${data.firstName}`
					})
						//window.location.replace refreshes page as we relocate
						//window.location.replace('/courses')
					})
				}
			})
		
			setEmail("");
			setPassword("");
	
	}



	return (

		user.email || willRedirect
		?
		<Redirect to='/' /> 
		:
		<Container>
		<Row className="mt-5">
			<Col>
			<h1>Login Here</h1>
			<Form className="mt-5" onSubmit = {e => loginUser(e)}>
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Login using email" value={email} onChange={e => {setEmail(e.target.value)}} required />

					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Password" value={password} onChange={e => {setPassword(e.target.value)}} required />
				</Form.Group>
				{
					isActive
					? <Button variant="dark" className="color-button" type="submit" block>Login</Button>
					: <Button variant="dark" className="color-button" disabled block>Login</Button>
				}
			</Form>		
			</Col>
		</Row>
		</Container>
		)


}
