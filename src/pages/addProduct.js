import React, {useState, useEffect, useContext} from 'react'
import {Form, Button, Row, Col} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'
import UserContext from '../userContext'
import {Container} from 'react-bootstrap'

export default function AddProduct(){

	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [img, setImg] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)
	//get global user
	const {user} = useContext(UserContext);


	useEffect(() => {

		if(productName !== "" && description !== "" && price !== "" && img !== ""){
		
				setIsActive(true);

		} else {
	
				setIsActive(false);

		}


	},[productName, description, price, img]);

	//console.log(localStorage.token)

	function addProduct(e){

		e.preventDefault();
		const token = localStorage.getItem('token');

		fetch('https://mighty-crag-87517.herokuapp.com/api/products',{
			method: "POST",
			headers: {
				//if your request has a body, you have to pass the content type header
				"Content-Type": "application/json",
				//if your request needs a token you have to pass the authorization
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				img: img
			})
		}).then(res => res.json())
			.then(data => {
				console.log(data)
				if(data.message){
					Swal.fire({
						icon: "error",
						title: "Failed to add Product",
						text: data.message
					})
				} else {

					setWillRedirect(true);

					Swal.fire({
						icon: "success",
						title: "Product Added Successfully",
						text: "Now available to our shop"
					})
				}
			})
			setProductName("");
			setDescription("");
			setPrice("");
			setImg("");
	}

	return (
		//user.isAdmin === true || user.isAdmin !== null
		user.isAdmin || willRedirect
		?
		<Container>
		<Row className="mt-5">
			<Col>
			<h1>Add a Product</h1>
		<Form onSubmit = {e => addProduct(e)}>
			<Form.Group>
				<Form.Label>Product Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter product here" value={productName} onChange={e => {setProductName(e.target.value)}} required />

				<Form.Label>Product Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter description" value={description} onChange={e => {setDescription(e.target.value)}} required />

				<Form.Label>Price:</Form.Label>
				<Form.Control type="Number" placeholder="Enter price" value={price} onChange={e => {setPrice(e.target.value)}} required />
				<Form.Label>Image:</Form.Label>
				<Form.Control type="text" placeholder="Upload Image" value={img} onChange={e => {setImg(e.target.value)}} required />
			</Form.Group>
			{
				isActive
				? <Button className="color-button" type="submit">Add Product</Button>
				: <Button className="color-button" disabled>Add Product</Button>
			}
		</Form>	
		</Col>
		</Row>
		</Container>
		:
		<Redirect to ='/login' />
		)
}