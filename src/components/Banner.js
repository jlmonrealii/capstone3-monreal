import React from 'react';
import '../App.css';


import {Link} from 'react-router-dom'
//import bgimage from '../images/img5.jpeg'

import {Jumbotron, Button, Row, Col} from 'react-bootstrap'

//turn this into a carousel if kaya
export default function Banner ({bannerProp}) {
	return(
		<Row>
			<Col>
				<Jumbotron className="jumbotron">
					<h1 className="hJumbo">{bannerProp.title}</h1>
					<p>{bannerProp.description}</p>
					{/*<Link to={bannerProp.destination} className="btn btn-primary">{bannerProp.label}</Link>*/}
					{/*<Button as={Link} to={bannerProp.destination} variant="primary">{bannerProp.label}</Button> */}
				</Jumbotron>
			</Col>
		</Row>
		)
}