import React from 'react';

//react-bootstrap components

import {Row, Col, Card} from 'react-bootstrap'
import {Container} from 'react-bootstrap'

export default function Featured(obj){
	//console.log(obj)
	return (
		<Container className="mt-5 mb-5">
			
		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn from Home</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum laborum magna cillum eiusmod ullamco aliqua proident Mollit eiusmod eu nisi laborum officia officia quis labore aliquip reprehenderit ullamco.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum laborum magna cillum eiusmod ullamco aliqua proident aliquip do cillum amet fugiat sit. Ea eiusmod pariatur. Cupidatat dolore qui nisi est 
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Be Part Of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Lorem ipsum laborum magna cillum eiusmod ullamco aliqua proident aliquip do cillum amet fugiat sit. Ea eiusmod pariatur. Ut sunt in aliqua. Lorem ipsum ut nisi et labore dolore duis in quis labore duis esse adipisicing.Commodo aliqua aliqua excepteur ad cillum cupidatat do dolore.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		</Container>
		)
}