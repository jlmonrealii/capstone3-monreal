import React, {useContext} from 'react';
import '../App.css';

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

import {Container} from 'react-bootstrap'
import {NavLink, Link} from 'react-router-dom'

import UserContext from '../userContext'
import image6 from '../images/garaje.png'



export default function NavBar(){

	
	const {user,unsetUser, setUser} = useContext(UserContext)
	console.log(unsetUser)

	
	function logout(){
		unsetUser()

		setUser({
			email: null,
			isAdmin: null
		})
		window.location.replace('/login')
	}


	return(
		<Navbar className="color-nav" variant="dark" expand="lg" sticky="top">
			<Navbar.Brand as={Link} to="/">
				<img src={image6} width="150" height="40" className="d-inline-block align-top navlogo" alt="Garaje"/>
			</Navbar.Brand>

			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="justify-content-center center-navbar">
					<Nav.Link as={NavLink} to="/">HOME</Nav.Link>
					<Nav.Link as={NavLink }to="/products">PRODUCTS</Nav.Link>
					{
						user.email
						?
							user.isAdmin
							?
							<>
							<Nav.Link as={NavLink} to='/addproduct'>ADD PRODUCTS</Nav.Link>
							<Nav.Link onClick={logout}>LOGOUT</Nav.Link>
							</>
							: <Nav.Link onClick={logout}>LOGOUT</Nav.Link>
						: <>
						<Nav.Link as={NavLink }to="/register">REGISTER</Nav.Link>
						<Nav.Link as={NavLink }to="/login">LOGIN</Nav.Link>
						</>

					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		)
}