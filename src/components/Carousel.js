import React from 'react';
import '../App.css';
import image from '../images/img1.jpeg'
import image2 from '../images/img2.jpeg'
import image3 from '../images/img3.jpeg'

import {Link} from 'react-router-dom'
import {Button} from 'react-bootstrap'

import Carousel from 'react-bootstrap/Carousel'


export default function CarouselHome({carouselProp}){

return(
		<Carousel fade className="carousel">
		  <Carousel.Item className="item">
		    <img
		      className="d-block w-100"
		      src={image}
		      alt="First slide"
		    />
		    <Carousel.Caption className="carousel-caption">
		      <h3>JOIN OUR COMMUNITY</h3>
		      <p>Be part of the gwapo & gwapa community and join our festivals and shows!</p>
		      <Button as={Link} to='/login' className="color-button" size="lg" variant="dark">LOGIN</Button> 
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item className="item">
		    <img
		      className="d-block w-100"
		      src={image3}
		      alt="Second slide"
		    />

		    <Carousel.Caption className="carousel-caption">
		      <h3>FREE SHIPPING!</h3>
		      <p>Free Shipping on your first order nationwide!</p>
		      <Button as={Link} to='/products' className="color-button" size="lg" variant="dark">VIEW PRODUCTS</Button> 
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item className="item">
		    <img
		      className="d-block w-100"
		      src={image2}
		      alt="Third slide"
		    />

		    <Carousel.Caption className="carousel-caption">
		      <h3>LOADS OF DISCOUNTS!</h3>
		      <p>Be sure to always check our store for discounts!</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>

)
}


