import React from 'react';

import Banner from '../components/Banner'

export default function NotFound(){

	let bannerContent = {
		title: "Page Cannot Be Found",
		description: "No Gwapo here",
		label: "Return to Home page",
		destination: "/"
	}

	return (
		<>
			<Banner bannerProp={bannerContent} />
		</>
		)
}