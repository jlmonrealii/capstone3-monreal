import React, {useState, useEffect, useContext} from 'react'

import {Table, Button} from 'react-bootstrap'

import Banner from '../components/Banner'
import Product from '../components/Product' 
import {Row, Col} from 'react-bootstrap'  
import {Container} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'

import UserContext from '../userContext'

export default function Products(){
	//console.log(courses)
	//state to store our courses
	const[allProducts, setAllProducts] = useState([])
	const[activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] =  useState(0)//useState("") 
	const {user} = useContext(UserContext)

	const [viewProd, setViewProd] = useState({})

	const [show, setShow] = useState(false);

	  const handleClose = () => setShow(false);
	  const handleShow = () => setShow(true);

	useEffect(() => {

		fetch('https://mighty-crag-87517.herokuapp.com/api/products')
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setAllProducts(data.data)//kasi yung array ng data ang kukunin kaya .data (based sa console)

			let productsTemp = data.data
			/*temporary aray to hold filtered items. only active courses*/
			let tempArray = productsTemp.filter(product => {
				//console.log(course)
				return product.isActive === true
			})

			setActiveProducts(tempArray)

		})	

	}, [update])


	function archive(productId){
		//console.log("Hello from Archive")
		fetch(`https://mighty-crag-87517.herokuapp.com/api/products/archive/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUpdate({})

		})
	}



	function activate(productId){
		//console.log("Hello from Activate")
		fetch(`https://mighty-crag-87517.herokuapp.com/api/products/active/${productId}`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUpdate({})
		})

	}

	function passProductId(productId){
		console.log(productId)

		localStorage.setItem('editId', productId)


	}

	function viewProduct(product){
		//console.log(product)

		setViewProd(product)
	}

	console.log(viewProd);
	//console.log(typeof [])

	/*our course components should only see active courses*/
	let productComponents = activeProducts.map(product => {
		return (

		<Product key={product._id} productProp={product}/>

			)
	})

	//map out all courses to create rows for our tables
	let productRows = allProducts.map(product => {
		return (
			<tr key={product._id}>
				<td>{product._id}</td>
				<td>{product.productName}</td>
				<td>
					<Button variant="primary" onClick={()=>
						{handleShow()
						viewProduct(product)}
						}>
				        View Product
				      </Button>

					<Button variant="primary" className="mx-2" as={Link} to={`/updateProduct?Id=${product._id}&color=hello`} onClick={()=>passProductId(product._id)} >Edit Product</Button>
				</td>
				<td className={product.isActive ? "text-success": "text-danger"}>{product.isActive ? "Active" : "Inactive"}</td>
				<td>
					{
						product.isActive
						?
						<Button variant="danger" className="mx-2" onClick={()=>archive(product._id)}>Archive</Button>
						:
						<Button variant="success" className="mx-2" onClick={()=>activate(product._id)}>Activate</Button>
					}
			 	</td>
			</tr>
			)
	})





	let bannerContent = 
	        {
	          title: "GARAJE PRODUCTS",
	          description: "CAR PARTS PARTS AND ACCESSORIES",
	          label: "Login",
	          destination: "/login"
	        }


	return (

		!user.isAdmin
		? 
		<>
		<Banner bannerProp={bannerContent} />
		<Container className="mt-5 mb-5">
			<Row xs={1} md={3} className="mt-5 mb-5">
			{productComponents}
			</Row>
			
		</Container>
		</>
		:
		<>
			<Container>
			<Row className="mt-5">
			<Col>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table	striped bordered hover>

				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Products</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productRows}
				</tbody>
			</Table>
			</Col>
			</Row>
			<Modal
			  show={show}
			  onHide={handleClose}
			  backdrop="static"
			  keyboard={false}
			>
			  <Modal.Header closeButton>
			    <Modal.Title>{viewProd.productName}</Modal.Title>
			  </Modal.Header>
			  <Modal.Body>
			    Description: {viewProd.description}
			  </Modal.Body>
			  <Modal.Body>
			    Price: {viewProd.price}
			  </Modal.Body>
			  <Modal.Footer>
			    <Button variant="secondary" onClick={handleClose}>
			      Close
			    </Button>
			  </Modal.Footer>
			</Modal>

			</Container>
		</>


	)
}