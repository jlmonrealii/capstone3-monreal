import React, {useState, useEffect, useContext} from 'react'
import {Form, Button, Row, Col} from 'react-bootstrap'
import Swal from 'sweetalert2'
import {Redirect} from 'react-router-dom'
import UserContext from '../userContext'
import {Container} from 'react-bootstrap'

export default function UpdateProduct(){

	const [productName, setProductName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [img, setImg] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)
	const [allProducts, setAllProducts] = useState([])
	const [activeProducts, setActiveProducts] = useState([])
	const [update, setUpdate] =  useState(0)//


	const urlParams = window.location.search;
	//console.log(urlParams)

	const queryUrl = new URLSearchParams(urlParams);
	// console.log(queryUrl)
	//get global user
	const {user} = useContext(UserContext);
	const productId = queryUrl.get('Id')
	// console.log(productId);

	const color = queryUrl.get('color')
	// console.log(color);

	const prodId = localStorage.getItem('editId')
	// console.log(prodId)


	useEffect(() => {

		if(productName !== "" && description !== "" && price !== "" && img !== ""){
		
				setIsActive(true);

		} else {
	
				setIsActive(false);

		}


	},[productName, description, price, img]);



	function updateProduct(e){
		e.preventDefault();

		const token = localStorage.getItem('token');

		fetch(`https://mighty-crag-87517.herokuapp.com/api/products/update/${productId}`,{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				img: img
			})
		}).then(res => res.json())
			.then(data => {
				console.log(data)
				if(data.message){
					Swal.fire({
						icon: "success",
						title: "Update Status:",
						text: data.message
					})
				} else {

					setWillRedirect(true);

					Swal.fire({
						icon: "error",
						title: "Product Update Failed",
						text: "Try Again Later"
					})
				}
			})
			setProductName("");
			setDescription("");
			setPrice("");
			setImg("");
	}

	return (
		user.isAdmin || willRedirect
		?
		<Container>
		<Row className="mt-5">
			<Col>
			<h1>Update a Product</h1>
		<Form onSubmit = {e => updateProduct(e)}>
			<Form.Group>
				<Form.Label>Product Name:</Form.Label>
				<Form.Control type="text" placeholder="Enter product here" value={productName} onChange={e => {setProductName(e.target.value)}} />

				<Form.Label>Product Description:</Form.Label>
				<Form.Control type="text" placeholder="Enter description" value={description} onChange={e => {setDescription(e.target.value)}} />

				<Form.Label>Price:</Form.Label>
				<Form.Control type="Number" placeholder="Enter price" value={price} onChange={e => {setPrice(e.target.value)}} />
				<Form.Label>Image:</Form.Label>
				<Form.Control type="text" placeholder="Upload Image" value={img} onChange={e => {setImg(e.target.value)}} />
			</Form.Group>
			{
				<Button className="color-button" type="submit">Update Product</Button>
			}
		</Form>	
		</Col>
		</Row>
		</Container>
		:
		<Redirect to ='/login' />

		)


}


