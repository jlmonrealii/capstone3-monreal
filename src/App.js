import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import {Container} from 'react-bootstrap'

//react-router-dom
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'


//COMPONENTS
import NavBar from './components/NavBar'
import Product from './components/Product'
import Banner from './components/Banner'
import Featured from './components/Featured'

//PAGES
import Register from './pages/register'
import Login from './pages/login'
import Home from './pages/home'
import Products from './pages/products'
import NotFound from './pages/notFound'
import AddProduct from './pages/addProduct'
import UpdateProduct from './pages/updateProduct'


import {UserProvider} from './userContext'



function App() {

 const[user, setUser] = useState({

      email: localStorage.getItem('email'),
      isAdmin: localStorage.getItem('isAdmin') === "true"

    })
 
  console.log(user)

    function unsetUser(){
    localStorage.clear() 
  }

  return (
    <>
          <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            <NavBar />
            
              <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/products" component={Products} />
                <Route exact path="/addproduct" component={AddProduct} />
                <Route exact path="/updateProduct" component={UpdateProduct} />
                <Route component={NotFound} />
              </Switch>
            
            </Router>
          </UserProvider>
        </>
    )
}

export default App;
